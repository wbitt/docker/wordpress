# Manual Build and Auto Build instructions:

## Manual build:
```
docker build -t local/wordpress:5.3.2-php-7.3-apache-2.4-devel  .
```

## Automated Build:

### Step 1: 
Make sure that this repo is connected to [https://cloud.docker.com](https://cloud.docker.com)

### Step 2: (For repos in github)
Create a new build rule for this automated build. 

**Note:** This applies only for GitHub repo linked with DockerHub. DockerHub does not allow gitlab repos to connect to it. For repos in gitlabtlab, we need to use gitlab's built in ci system.

In [https://cloud.docker.com](https://cloud.docker.com), create a new rule as follows:
**For master**
* Source Type: Branch
* Source: master
* Docker Tag: latest
* Dockerfile location: Dockerfile
* Build Context: / 
* Autobuild: On
* Build Caching: On

**For tag**
* Source Type: Tag
* Source: /^wordpress-(.*)$/
* Docker Tag: {\1}
* Dockerfile location: Dockerfile
* Build Context: / 
* Autobuild: On
* Build Caching: On


**Note:** The regex `/^[0-9.]+.*$/` allows you to build docker images automatically for git tags such as: `5.2.4`, or `5.2.4-test` or `5.2.4.v-1.2` , etc.
**Note:** The regex  `/^wordpress-(.*)$/` allows you to build docker images automatically for git tags such as: `wordpress-5.3.2-php-7.3-apache-2.4`

### Step 2: (For repos in gitlab)
DockerHub does not allow gitlab repos to connect to it. For repos in gitlabtlab, we need to use gitlab's built in ci system to be able to build and push image to DockerHub.

* Create a new token from DockerHub interface, e.g `GITLAB_CI_ACCESS`
* In the gitlab interface for the repository, create a new environment variable `DOCKER_TOKEN` under CI/CD settings, and copy the value of the token obtained from DockerHub.
* Create a `.gitlab-ci.yml` file in the project root directory of your gitlab repo, with the following contents:

```
variables:
  # CONTAINER_IMAGE is the image's URL you usually use to `pull` your image.
  # e.g. docker pull witline/wordpress 
  
  CONTAINER_IMAGE: witline/wordpress
  DOCKER_USER: kamranazeem

build-and-push-image:
  stage: build
  # This "image" is used by the runner
  image: docker:latest
  services:
  - docker:dind
  # Inside the runner , "docker in docker" runs as a service,
  #   so docker commands can find the docker daemon and do the `build` and `push`.
  script:
    - docker login -u ${DOCKER_USER} -p ${DOCKER_TOKEN}
    - docker build -t ${CONTAINER_IMAGE}:${CI_COMMIT_SHORT_SHA} ${CONTAINER_IMAGE}:${CI_COMMIT_REF_NAME} -t ${CONTAINER_IMAGE}:latest .
    - docker push ${CONTAINER_IMAGE}:${CI_COMMIT_SHORT_SHA}
    - docker push ${CONTAINER_IMAGE}:${CI_COMMIT_REF_NAME}
    - docker push ${CONTAINER_IMAGE}:latest 
```


### Step 3:

* git add 
* git commit
* git tag wordpress-5.3.2-php-7.3-apache-2.4
* git push origin master && git push origin master --tags

**Note:** `git push --tags` does not push commits to remote. So you need a separate `git push` as well.


# Manage git tags:

## List local tags:
```
$ git tag
wordpress-5.1.1-php-7.3-apache-2.4-v-1.0
wordpress-5.1.1-php-7.3-apache-2.4-v-2.0
wordpress-5.1.1-php-7.3-apache-2.4-v-2.1
wordpress-5.1.1-php-7.3-apache-2.4-v-2.2
wordpress-5.2.1-php-7.3-apache-2.4-v-1.0
wordpress-5.2.2-php-7.3-apache-2.4-v-1.0
wordpress-5.2.2-php-7.3-apache-2.4-v-1.1
wordpress-5.2.2-php-7.3-apache-2.4-v-1.2-devel
wordpress-5.2.2-php-7.3-apache-2.4-v-1.3
wordpress-5.2.3-php-7.3-apache-2.4-v-1.3
wordpress-5.2.4-php-7.3-apache-2.4
wordpress-5.3.2-php-7.3-apache-2.4
```

## List remote tags:
```
$ git ls-remote --tags

From git@gitlab.com:witline/wordpress/wordpress-docker-image.git
408c5ca513f9a4485953cc7f7a90cc2d8e92bb57	refs/tags/wordpress-5.1.1-php-7.3-apache-2.4-v-1.0
6dfbce79b3eafc62e722f0dfedf32f1214bf5ff9	refs/tags/wordpress-5.1.1-php-7.3-apache-2.4-v-2.0
9ab51ae5995926fc2b9c2c12ef2607bc0c8aae16	refs/tags/wordpress-5.1.1-php-7.3-apache-2.4-v-2.1
0edc52d245634770608c820bcacdce47284286cc	refs/tags/wordpress-5.1.1-php-7.3-apache-2.4-v-2.2
26afcccdfe4c248e807ca233f429645766871a72	refs/tags/wordpress-5.2.1-php-7.3-apache-2.4-v-1.0
0aa0896164a0e8d93628039eee7dcb2eeff5e41b	refs/tags/wordpress-5.2.2-php-7.3-apache-2.4-v-1.0
84619aa66aedc79c69b1099a0d5a6800371476e9	refs/tags/wordpress-5.2.2-php-7.3-apache-2.4-v-1.1
b5ed8332e5481ae80d098b967cb980afe1500c1c	refs/tags/wordpress-5.2.2-php-7.3-apache-2.4-v-1.2-devel
155b12f95542ee88f449e028510349371e551f1c	refs/tags/wordpress-5.2.2-php-7.3-apache-2.4-v-1.3
f68554b124ce38887687dcae991515c36d0c656c	refs/tags/wordpress-5.2.3-php-7.3-apache-2.4-v-1.3
aad5c5c3e3268f2347fc0749b9942cb18bfdf88b	refs/tags/wordpress-5.2.4-php-7.3-apache-2.4
2efed0184fc6529b65dabbd93cab728418e95b99	refs/tags/wordpress-5.3.2-php-7.3-apache-2.4
```

## Delete git tag from local repository: 
```
git tag -d wordpress-5.2.2-php-7.3-apache-2.4-v-1.2-devel
```

## Delete git tag from remote repository: 
```
git push origin --delete wordpress-5.2.2-php-7.3-apache-2.4-v-1.2-devel
```


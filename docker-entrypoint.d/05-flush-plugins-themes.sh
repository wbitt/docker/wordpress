#!/bin/bash

FLUSH_PLUGINS_THEMES=${FLUSH_PLUGINS_THEMES:-false}

if [ "${FLUSH_PLUGINS_THEMES}" == "true" ]; then 
  echo "FLUSH_PLUGINS_THEMES is set to 'true'"
  echo "Proceeding to empty wp-content/plugins and wp-content/themes directories"
  rm -fr ${APACHE_DOC_ROOT}/wp-content/plugins/* 
  rm -fr ${APACHE_DOC_ROOT}/wp-content/themes/* 
  echo "FLUSH complete for plugins and themes."
else
  echo "FLUSH_PLUGINS_THEMES is set to 'false'. Nothing to do"
fi 

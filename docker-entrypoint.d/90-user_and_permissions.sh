#!/bin/bash

# This script does two things:
# - makes sure that there is an OS user with the uid passed to the container.
# - sets the ownership of APACHE_DOC_ROOT location to the uid/gid.

# If APACHE_RUN_USER and APACHE_RUN_GROUP are set,  
#   then we need to ensure that a user with that ID exists in the container,
#   otherwise Apache will complain that a user matching the uid does not exist.

echo

# If APACHE_RUN_* are empty, set them to default value of www-data.
APACHE_RUN_USER=${APACHE_RUN_USER:-www-data}
APACHE_RUN_GROUP=${APACHE_RUN_GROUP:-www-data}

if [ -z "${APACHE_RUN_USER}" ] || [ -z  "${APACHE_RUN_GROUP}" ] ; then

  echo "APACHE_RUN_USER and/or APACHE_RUN_GROUP are empty / unbound / undefined. Nothing to do ..."
else
  echo "Found APACHE_RUN_USER=${APACHE_RUN_USER} and APACHE_RUN_GROUP=${APACHE_RUN_GROUP} ..."
  echo "Setting up APACHE to run as APACHE_RUN_USER=${APACHE_RUN_USER}"
  echo "  and APACHE_RUN_GROUP=${APACHE_RUN_GROUP}"

  # First we remove any # signs from the APACHE_RUN_USER and APACHE_RUN_GROUP variables.

  user="${APACHE_RUN_USER}"
  group="${APACHE_RUN_GROUP}"

  # strip off any '#' symbol from the user and group variables
  pound='#'
  user="${user#$pound}"
  group="${group#$pound}"

  # Check if it is a number, or a username:

  re='^[0-9]+$'
  if [[ ${user} =~ $re && ${group} =~ $re ]] ; then
     # echo "$user and $group are numeric"
     # In this case, create a new user with this ID.
     # It is important to do this, otherwise apache complains about uid not matching to an existing user.
    
     echo "Attempting to create a user 'site-owner' with UID ${user} and GID ${group} ..."
     # The (OR TRUE) (|| true) is added because it is possible that the container,
     #   has already run once, and the user already exists. 
     #   This logic needs to improve though.

     # In case the user passes the number 33 - which is system default 'www-data',
     #   then the groupadd and useradd commands below will simply fail,
     #   but the '|| true' will prevent the script from failing,
     #   which is necessary for the container to start correctly.

     groupadd -g ${group} site-owner || true
     useradd -u ${user} -g ${group}  site-owner  || true


  else

     echo "User $user is NOT numeric, so it must be name of a system user."
     echo "It is expected that the operator knows about this,"
     echo "  and knows what he/she is doing. So we do nothing about it ..."

    
  fi

  # Run the chown command in both cases:
  echo "Changing ownership of ${APACHE_DOC_ROOT} to ${user} ${group} - recursively ..." 

  # Since gitlab has special problem with colon (:), 
  #   the chown and chgrp commands are used separately.
  chown -R ${user} ${APACHE_DOC_ROOT} && echo "chown completed successfully."
  chgrp -R ${group} ${APACHE_DOC_ROOT} && echo "chgrp completed successfully."
  

fi 


#!/bin/bash

########### START - user variables #####################
#

# The user of this image is expected to pass GIT_USER and GIT_TOKEN variables 
#   as environment variables "if" git repos need to be downloaded.
# If running through CI/CD, then these two variables must be part of 
#   the CI/CD settings.  
#
# GIT_USER and GIT_TOKEN must "NEVER" be setup in this script.

#
########### END - user variables #####################


########### START - system variables #####################
#

THEMES_SOURCE_DIR="/usr/src/themes"
PLUGINS_SOURCE_DIR="/usr/src/plugins"

THEMES_DOWNLOAD_FILE="${THEMES_SOURCE_DIR}/download.list"
PLUGINS_DOWNLOAD_FILE="${PLUGINS_SOURCE_DIR}/download.list"

THEMES_GIT_REPOS_FILE="${THEMES_SOURCE_DIR}/gitrepos.list"
PLUGINS_GIT_REPOS_FILE="${PLUGINS_SOURCE_DIR}/gitrepos.list"


APACHE_DOC_ROOT="/var/www/html"

THEMES_TARGET_DIR="${APACHE_DOC_ROOT}/wp-content/themes/"
PLUGINS_TARGET_DIR="${APACHE_DOC_ROOT}/wp-content/plugins/"

# This is also used in Dockerfile. 
# So if you change here, you have to change there as well.
ENTRYPOINT_DIRECTORY=/docker-entrypoint.d

#
########### END - system variables #####################


######################################################

# Before we run our custom entry point logic, we must run the main docker-entrypoint.sh script in wordpress container.
# Since we will overwrite the entrypoint ourselves and use our custom script, 
# it is imporant to source the main entrypoint in this script, let it do it's thing and then we run our script.

# echo "DEBUG: /var/www/html - BEFORE:"
# ls -lR  /var/www/html

if [ -r /usr/local/bin/docker-entrypoint.sh ]; then
  source /usr/local/bin/docker-entrypoint.sh
fi

# echo "DEBUG: /var/www/html - AFTER:"
# ls -lR /var/www/html

############ START - Custom Entrypoint logic ###############

echo
echo "----> Running  wordpress-custom-entrypoint.sh script <-------"
echo

# Provide backward compatibility to older version of script
if [ ! -z ${GITHUB_USER+x} ] && [ ! -z ${GITHUB_TOKEN+x} ]; then
  GIT_USER=${GITHUB_USER}
  GIT_TOKEN=${GITHUB_TOKEN}
fi


# Reference: https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash
if [ -z ${GIT_USER+x} ] || [ -z ${GIT_TOKEN+x} ]; then
  echo "GIT_USER and/or GIT_TOKEN are found to be empty/unset."
  echo "Themes and plugins from private git repositories will not be downloaded becasue of this."
  USE_GIT=0
else
  echo "Found git credentials for user ${GIT_USER} ... Will use 'git' to pull git repositories ..."

  # GIT_USER is plaintext/regular in app.env (eg. john)
  # GIT_TOKEN is base64 encoded when picked from app.env . 
  # It needs to be decoded before it is used.
  
  # GIT_CREDENTIALS=${GIT_USER}:${GIT_TOKEN}
  GIT_CREDENTIALS=${GIT_USER}:$(echo ${GIT_TOKEN} | base64 -d)

  # echo "Debug - GIT_CREDENTIALS are set as: ${GIT_CREDENTIALS}"

  # We will use: git clone https://user:token@github.com/org/repo.git
  # or: git clone https://$GIT_CREDENTIALS@github.com/org/repo.git

  USE_GIT=1
fi

echo


# Only run the theme and plugin copying scipts, if there is a wp-content directory under APACHE_DOC_ROOT. 
if [ -d ${APACHE_DOC_ROOT}/wp-content ]; then

  echo "Calling/source-ing all custom scripts from the ${ENTRYPOINT_DIRECTORY} directory - if there are any ...." 
  for SCRIPTFILE in $(find ${ENTRYPOINT_DIRECTORY} -name "*.sh" | sort -n); do
    echo
    echo "=====> Running script file: ${SCRIPTFILE} ..."
    source ${SCRIPTFILE} 
  done

else

  echo "There is no wordpress content wp-content/ under APACHE_DOC_ROOT: $APACHE_DOC_ROOT."
  echo "So, skipping all scripts under ${ENTRYPOINT_DIRECTORY} which help setup themes and plugins ..."
fi 


# unset all sensitive environment variables. 
# Though unset only works for variables defined in this (script) session.
# Not much useful:
unset GIT_USER GIT_TOKEN GIT_CREDENTIALS 

# Try exporting them with null values:
export GIT_USER=""
export GIT_TOKEN=""
export GIT_CREDENTIALS=""



echo
echo "----> Finished running wordpress-custom-entrypoint.sh script <-------"
echo

# Run any other commands passed as arguments in CMD in Dockerfile, such as apache2-foreground
exec "$@"
